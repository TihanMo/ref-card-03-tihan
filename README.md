# Refcard 03

Refcard 03 is a example App that allows students to learn and do experiments.

This App is based on Java and it displays jokes. It teaches students how to utilise Java with databases and in this example how to create CI/CD Pipelines.

## Prerequisites

Before you begin, ensure you have met the following requirements:
* You have installed the latest version of Java
* You have a Windows machine. (Other OS's should work but weren't tested)
* You have read README.md of this project.
* You have a gitlab account with full access to CI/CD Pipelines.
* You have access to AWS Services.

## Installing Refcard 03

To install Refcard 03, follow these steps:

```
git clone https://gitlab.com/TihanMo/ref-card-03-tihan.git
```
In project folder
```
mvn clean package
```
## Using Refcard 03
To use Refcard 03, follow these steps:
```
docker build -t refcard-03 .
```

```
docker run refcard-03
```
```
java -jar ref-card-03-0.0.1-SNAPSHOT.jar
```

## Using Pipelines with AWS

1. Go to your Gitlab Repository and adjust the following Variables in the CI/CD settings:
    * AWS_ACCESS_KEY_ID
    * AWS_DEFAULT_REGION
    * AWS_SECRET_ACCESS_KEY
    * AWS_SESSION_TOKEN
    * CI_AWS_REGISTRIY
2. Set up an ECR
3. Push latest Docker image of Refcard 03
4. Create an RDS
5. Create a Database
6. Set up an ECS Cluster
7. Create a Task Definition
8. Adjust the Environment Variables (DB_URL, DB_USERNAME, DB_PASSWORD)
9. Go to your Gitlab Repository and run the Pipeline


## Contributing to Refcard 03
To contribute to Refcard03, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.

Alternatively see the GitHub documentation on [creating a pull request](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## Contributors

Thanks to the following people who have contributed to this project:

* [@giacii](https://gitlab.com/Giaci) 📖

## Contact

If you want to contact me you can reach me at [tihan.morrol@lernende.bbw.ch](tihan.morrol@lernende.bbw.ch)

## License

This project uses the following license: Public Domain.
